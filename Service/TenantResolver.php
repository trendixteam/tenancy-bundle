<?php
/**
 * Created by PhpStorm.
 * User: jose
 * Date: 23/5/17
 * Time: 17:11
 */

namespace Trendix\TenancyBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;

class TenantResolver
{
    protected $container;
    protected $em;
    protected $requestStack;

    /**
     * TenantResolver constructor.
     * @param $container
     * @param $em
     */
    public function __construct(Container $container, EntityManager $em, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public function getCurrentTenantId()
    {
        $baseUrl = $this->requestStack->getCurrentRequest()->getHost();
        $subdomain = explode('.', $baseUrl)[0];
        $tenant = $this->em->getRepository('TrendixTenancyBundle:Tenant')->findOneBy([
            'subdomain' => $subdomain
        ]);
        return $tenant->getId();
    }


}

