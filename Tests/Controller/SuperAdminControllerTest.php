<?php

namespace Trendix\TenancyBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SuperAdminControllerTest extends WebTestCase
{
    public function testListcustomers()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/list_customers');
    }

    public function testNewcustomer()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'new_customer');
    }

    public function testShowcustomer()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/show_customer');
    }

}
