<?php

namespace Trendix\TenancyBundle\Controller;

use Trendix\TenancyBundle\TenantFilter;

class BaseController extends \Trendix\AdminBundle\Controller\BaseController
{
    public $tenant;

    /**
     * Inicializa Doctrine
     */
    public function initializeEntityManager()
    {
        parent::initializeEntityManager();
        $config = $this->em->getConfiguration();
        $config->addFilter('tenant_id', TenantFilter::class);
        $this->em->getFilters()->enable('tenant_id');

        $this->tenant = $this->get('trendix_tenancy.tenant_subscriber')->getCurrentTenantId();
        //$this->em = $this->get('trendix_tenancy.entity_manager');
    }
}