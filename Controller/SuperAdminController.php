<?php

namespace Trendix\TenancyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class SuperAdminController
 * @package Trendix\TenancyBundle\Controller
 * @Route("/super_admin")
 */
class SuperAdminController extends Controller
{
    /**
     * @Route("/list_customers", name="list_customers_super")
     */
    public function listCustomersAction()
    {
        return $this->render('TrendixTenancyBundle:SuperAdmin:list_customers.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/new_customer", name="new_customer_super")
     */
    public function newCustomerAction()
    {
        return $this->render('TrendixTenancyBundle:SuperAdmin:new_customer.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/show_customer", name="show_customer_super")
     */
    public function showCustomerAction()
    {
        return $this->render('TrendixTenancyBundle:SuperAdmin:show_customer.html.twig', array(
            // ...
        ));
    }

}
