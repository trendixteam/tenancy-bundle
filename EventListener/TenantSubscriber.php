<?php

namespace Trendix\TenancyBundle\EventListener;


use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Trendix\TenancyBundle\Entity\TenancyEntity;
use Trendix\TenancyBundle\TenantFilter;

/**
 * Created by PhpStorm.
 * User: jose
 * Date: 22/6/17
 * Time: 11:29
 */
class TenantSubscriber implements EventSubscriberInterface
{
    protected $container;
    protected $em;
    protected $requestStack;

    /**
     * ExceptionSubscriber constructor.
     */
    public function __construct(Container $container, EntityManager $em, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return array(
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 10)
            ),
        );
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $session = new Session();
        $session->set('tenant_id', $this->getCurrentTenantId());
    }

    public function getCurrentTenantId()
    {
        $request = $this->requestStack->getCurrentRequest();
        if(!$request) {
            return 1;
        }
        $baseUrl = $request->getHost();
        $subdomain = explode('.', $baseUrl)[0];
        $tenant = $this->em->getRepository('TrendixTenancyBundle:Tenant')->findOneBy([
            'subdomain' => $subdomain
        ]);

        return $tenant->getId();
    }
}