<?php

namespace Trendix\TenancyBundle\Lista;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Trendix\TenancyBundle\TenantFilter;

/**
 * Created by PhpStorm.
 * User: jose
 * Date: 22/6/17
 * Time: 15:24
 */
class ListaFactory extends \Trendix\AdminBundle\Component\Lista\ListaFactory
{
    /**
     * ListaFactory constructor.
     */
    public function __construct(Container $container, EntityManager $em)
    {
        parent::__construct($container, $em);
        $config = $this->em->getConfiguration();
        $config->addFilter('tenant_id', TenantFilter::class);
        $this->em->getFilters()->enable('tenant_id');
    }
}