<?php

namespace Trendix\TenancyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Addsthe needed tenantId attribute and methods to an Entity
 *
 */
trait TenancyEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="tenant_id", type="integer")
     */
    protected $tenantId;



    /**
     * @return int
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * @param int $tenantId
     * @return TenancyEntity
     */
    public function setTenantId($tenantId)
    {
        $this->tenantId = $tenantId;
        return $this;
    }

}
