<?php

namespace Trendix\TenancyBundle\Repository;

use DateInterval;
use DatePeriod;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * TenancyRepository
 *
 * Extends EntityRepository functionalities in order to keep each data only available for its tenant
 */
class TenancyRepository extends EntityRepository
{
}
