<?php
/**
 * Created by PhpStorm.
 * User: jose
 * Date: 22/6/17
 * Time: 11:23
 */

namespace Trendix\TenancyBundle;

use Doctrine\ORM\Mapping\ClassMetaData,
    Doctrine\ORM\Query\Filter\SQLFilter;
use Symfony\Component\HttpFoundation\Session\Session;
use Trendix\TenancyBundle\Entity\TenancyEntity;

class TenantFilter extends SQLFilter
{

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if the entity has the TenancyEntity trait

        if (!$targetEntity->getReflectionClass()->hasMethod('setTenantId')) {
            return "";
        }

        $session = new Session();
        $tenant = $session->get('tenant_id');
        return $targetTableAlias.'.tenant_id = ' . $tenant;
    }
}